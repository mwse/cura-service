FROM golang:1.13-alpine as build

COPY . /src
WORKDIR /src

RUN go mod download
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o cura-service main.go

FROM registry.gitlab.com/mwse/docker/golang-prod
COPY --from=build /src/cura-service /bin/cura-service
VOLUME [ "/tmp" ]
EXPOSE 8080
ENTRYPOINT [ "/bin/cura-service" ]
