package render

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"image/png"
	"io"
	"os"

	gl "github.com/fogleman/fauxgl"
	"github.com/google/uuid"
	"github.com/pkg/errors"
)

const (
	fovy = 30 // vertical field of view in degrees
	near = 1  // near clipping plane
	far  = 10 // far clipping plane
)

var (
	down = gl.V(0, 1, 0) // down vector
)

type writeCounter int

func (c *writeCounter) Write(b []byte) (int, error) {
	written := int(len(b))
	*c = *c + writeCounter(written)
	return written, nil
}

func (c *writeCounter) Val() int64 {
	return int64(*c)
}

type RenderConfig struct {
	Width  int    `json:"width"`
	Height int    `json:"height"`
	Color  string `json:"color"`
}

func (r RenderConfig) hashKey() string {
	return fmt.Sprintf("%d-%d-%s", r.Width, r.Height, r.Color)
}

func (h *RenderHandler) renderCached(dest io.Writer, src io.ReadCloser, config RenderConfig) (int64, error) {
	filename, hash, err := h.saveObject(src)
	if err != nil {
		return 0, err
	}

	resultFile := fmt.Sprintf("/tmp/rendered-%s-%s.png", hash, config.hashKey())
	log := h.log.WithField("cache_file", resultFile)

	if fileExists(resultFile) {
		log.Debug("Serving from cache")
		f, err := os.Open(resultFile)
		if err != nil {
			return 0, err
		}
		return io.Copy(dest, f)
	}

	f, err := os.Create(resultFile)
	if err != nil {
		return 0, err
	}
	defer f.Close()

	count := new(writeCounter)
	w := io.MultiWriter(dest, f, count)
	log.Debug("Rendering model to response and file")
	return count.Val(), h.render(w, filename, config)
}

func (h *RenderHandler) render(dest io.Writer, filename string, config RenderConfig) error {
	mesh, err := gl.LoadSTL(filename)
	if err != nil {
		return errors.Wrap(err, "Failed loading model")
	}
	mesh.BiUnitCube()
	mesh.SmoothNormalsThreshold(gl.Radians(30))
	mesh.SetColor(gl.HexColor(config.Color))
	bb := mesh.BoundingBox().Size()
	centerZ := bb.Z / 2
	mesh.Transform(gl.Translate(gl.V(0, 0, centerZ)))

	plane := gl.NewPlane()
	plane.SetColor(gl.HexColor("ccc"))
	plane.Transform(gl.Scale(gl.V(bb.X*5, bb.Y*5, 1)))
	mesh.Add(plane)

	center := gl.V(0, 0, centerZ)
	eye := gl.V(0, bb.Y*-1*10, bb.Z)
	aspect := float64(config.Width) / float64(config.Height)
	matrix := gl.LookAt(eye, center, down).Perspective(fovy, aspect, near, far)

	light := gl.V(bb.X/2*-1, bb.Y*-2, bb.Z).Normalize()
	context := gl.NewContext(config.Width, config.Height)
	context.ClearColorBuffer()
	shader := gl.NewPhongShader(matrix, light, eye)
	context.Shader = shader
	context.DrawMesh(mesh)

	return png.Encode(dest, context.Image())
}

func (h *RenderHandler) saveObject(file io.Reader) (string, string, error) {
	uuid, err := uuid.NewUUID()
	if err != nil {
		return "", "", err
	}

	uuidFilename := fmt.Sprintf("/tmp/model-uuid-%s", uuid.String())
	hash := sha256.New()
	f, err := os.Create(uuidFilename)
	if err != nil {
		return "", "", err
	}
	defer f.Close()

	w := io.MultiWriter(hash, f)
	if _, err := io.Copy(w, file); err != nil {
		return "", "", err
	}

	id := hex.EncodeToString(hash.Sum(make([]byte, 0))[:])
	filename := fmt.Sprintf("/tmp/model-saved-%s", id)

	if fileExists(filename) {
		if err := os.Remove(uuidFilename); err != nil {
			h.log.WithError(err).Error("Failed removing temp file")
		}
		return filename, id, nil
	}

	if err := os.Rename(uuidFilename, filename); err != nil {
		return "", "", err
	}

	return filename, id, nil
}

func fileExists(filename string) bool {
	if _, err := os.Stat(filename); err != nil {
		return false
	}
	return true
}
