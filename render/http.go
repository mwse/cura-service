package render

import (
	"encoding/json"
	"net/http"

	"github.com/sirupsen/logrus"
)

type RenderHandler struct {
	log logrus.FieldLogger
}

func NewRenderHandler(log logrus.FieldLogger) *RenderHandler {
	return &RenderHandler{
		log: log,
	}
}

const (
	defaultWidth  = 400
	defaultHeight = 300
	defaultColor  = "ffea00"
)

func (h *RenderHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.NotFound(w, r)
		return
	}

	file, _, err := r.FormFile("model")
	if err != nil {
		httpErr(w, http.StatusBadRequest, err)
		return
	}

	config := RenderConfig{
		Width:  defaultWidth,
		Height: defaultHeight,
		Color:  defaultColor,
	}
	configJSON := r.FormValue("config")
	if configJSON != "" {
		if err := json.Unmarshal([]byte(configJSON), &config); err != nil {
			httpErr(w, http.StatusBadRequest, err)
			return
		}
	}

	h.log.WithFields(logrus.Fields{
		"width":  config.Width,
		"height": config.Height,
		"color":  config.Color,
	}).Debug("Starting render...")

	w.Header().Set("Content-Type", "image/png")
	if n, err := h.renderCached(w, file, config); err != nil {
		if n == 0 {
			httpErr(w, http.StatusInternalServerError, err)
		} else {
			h.log.WithError(err).Error("Failed serving image")
		}
	}
}

func httpErr(w http.ResponseWriter, status int, err error) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(struct {
		E string `json:"error"`
	}{err.Error()})
}
