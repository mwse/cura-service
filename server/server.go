package server

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/mwse/cura-service/api"
	"gitlab.com/mwse/cura-service/engine"
	"gitlab.com/mwse/cura-service/render"
)

func NewHTTPServer(log logrus.FieldLogger, addr, remoteHost string) (*http.Server, error) {
	mux := http.NewServeMux()

	ctx, cancelRunner := context.WithCancel(context.Background())
	runner, err := engine.NewRunner(ctx, log.WithField("component", "runner"), "http://"+remoteHost+"/engine/")
	if err != nil {
		cancelRunner()
		return nil, errors.Wrap(err, "Unable to create engine")
	}
	mux.Handle("/engine/", http.StripPrefix("/engine", runner))

	api := api.NewAPI(log.WithField("component", "api"), runner)
	mux.Handle("/api/", http.StripPrefix("/api", api))

	render := render.NewRenderHandler(log.WithField("component", "render"))
	mux.Handle("/render", http.StripPrefix("/render", render))

	s := &http.Server{
		Addr:    addr,
		Handler: WithTracing(log, mux),
	}
	s.RegisterOnShutdown(func() {
		cancelRunner()

		ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		defer cancel()
		fmt.Println("Shutting down engine...")
		if err := runner.Close(ctx); err != nil {
			fmt.Printf("Error on engine shutdown: %v\n", err)
			return
		}
		fmt.Println("Engine shut down.")
	})

	return s, nil
}
