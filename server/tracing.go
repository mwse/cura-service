package server

import (
	"net/http"
	"time"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
)

type loggingWriter struct {
	w      http.ResponseWriter
	log    logrus.FieldLogger
	status int
	start  time.Time
}

type RecordWriter interface {
	http.ResponseWriter
	Done()
}

func RequestLogger(w http.ResponseWriter, r *http.Request, log logrus.FieldLogger) RecordWriter {
	rec := &loggingWriter{
		w:      w,
		log:    log,
		status: http.StatusOK,
		start:  time.Now(),
	}

	reqID, err := uuid.NewUUID()
	if err != nil {
		return rec
	}

	reqFields := logrus.Fields{
		"request_id": reqID.String(),
		"method":     r.Method,
		"path":       r.URL.EscapedPath(),
		"remote":     r.RemoteAddr,
	}
	rec.log = rec.log.WithFields(reqFields)
	rec.log.Debug("Request started")
	return rec
}

func (w *loggingWriter) Header() http.Header {
	return w.w.Header()
}

func (w *loggingWriter) Write(buf []byte) (int, error) {
	return w.w.Write(buf)
}

func (w *loggingWriter) WriteHeader(statusCode int) {
	w.status = statusCode
	w.w.WriteHeader(statusCode)
}

func (w *loggingWriter) Done() {
	dur := time.Since(w.start)
	w.log.WithFields(logrus.Fields{
		"status": w.status,
		"dur":    dur.String(),
		"dur_ns": dur.Nanoseconds(),
	}).Debug("Request completed")
}

func WithTracing(log logrus.FieldLogger, handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		wRec := RequestLogger(w, r, log)
		handler.ServeHTTP(wRec, r)
		wRec.Done()
	})
}
