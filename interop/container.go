package interop

import (
	"context"
	"io"
)

type ContainerProvider interface {
	ContainerStart(ctx context.Context, image string, cmd []string) (string, <-chan error)
	ContainerStop(ctx context.Context, id string) error
	ContainerRemove(ctx context.Context, id string) error
	ContainerLogs(ctx context.Context, id string) io.ReadCloser
	ContainerState(ctx context.Context, id string) (ContainerState, error)
}

type ContainerRunner interface {
	ContainerProvider
	RemoteURL() string
}

type ContainerState struct {
	Running  bool
	ExitCode int
}
