package interop

import (
	"context"
	"encoding/json"
	"io"
)

type Engine interface {
	CreateJob() (Job, error)
	GetJob(id string) (Job, error)
	RemoveJob(id string) error
}

type Job interface {
	ID() string
	StoreFile(file string, content []byte)
	Start(ctx context.Context) error
	Stop(ctx context.Context) error
	Done() <-chan struct{}
	Result() io.Reader
	Logs(ctx context.Context) io.ReadCloser
	Progress() <-chan ProgressEntry

	json.Marshaler
}

type ProgressEntry struct {
	StepName     string  `json:"step_name"`
	StepProgress uint64  `json:"step_progress`
	StepMax      uint64  `json:"step_max"`
	Global       float64 `json:"global"`
}
