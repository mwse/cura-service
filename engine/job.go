package engine

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/google/uuid"
	"github.com/nxadm/tail"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/mwse/cura-service/interop"
)

var _ interop.Job = (*Job)(nil)

type Job struct {
	*http.ServeMux
	id         string
	files      map[string][]byte
	result     []byte
	provider   interop.ContainerRunner
	providerID string
	done       chan struct{}
	log        logrus.FieldLogger
	progress   chan interop.ProgressEntry

	lastErr  error
	errMutex sync.Mutex
}

func NewJob(log logrus.FieldLogger, provider interop.ContainerRunner) (*Job, error) {
	id, err := uuid.NewUUID()
	if err != nil {
		return nil, errors.Wrap(err, "unable to generate ID")
	}

	mux := http.NewServeMux()
	j := &Job{
		ServeMux: mux,
		id:       id.String(),
		files:    make(map[string][]byte),
		done:     make(chan struct{}),
		provider: provider,
		log:      log.WithField("job_id", id.String()),
		progress: make(chan interop.ProgressEntry),
	}

	mux.HandleFunc("/result", j.handleUploadResult)
	mux.HandleFunc("/", j.serveFile)

	return j, nil
}

func (j *Job) ID() string {
	return j.id
}

func (j *Job) StoreFile(file string, content []byte) {
	j.files[file] = content
}

func (j *Job) Result() io.Reader {
	return bytes.NewReader(j.result)
}

const jobTimeout = time.Minute * 10

func (j *Job) Start(ctx context.Context) error {
	cmd := containerCommand(j.provider.RemoteURL() + j.id)
	j.log.WithField("image", containerImage).Debug("Starting job")
	id, errChan := j.provider.ContainerStart(ctx, containerImage, cmd)
	select {
	case err := <-errChan:
		return errors.Wrap(err, "Failed to start engine")
	default:
	}

	j.providerID = id
	log := j.log.WithField("container_id", id)
	log.Info("Job started")

	go func() {
		defer func() {
			j.setDone()
			if err := j.provider.ContainerRemove(ctx, id); err != nil {
				log.WithError(err).Error("Failed removing container")
			}
		}()
		if err := <-errChan; err != nil {
			j.setError(errors.Wrap(err, "Failed starting container"))
			return
		}

		go j.processLogs(ctx, id)
		j.containerLifecycle(ctx, id)
	}()

	return nil
}

func (j *Job) containerLifecycle(ctx context.Context, id string) {
	log := j.log.WithField("container_id", id)
	timeout := time.After(jobTimeout)
	for {
		state, err := j.provider.ContainerState(ctx, id)
		if err != nil {
			j.setError(errors.Wrap(err, "Failed querying state"))
			return
		}
		if !state.Running {
			if state.ExitCode != 0 {
				j.setError(fmt.Errorf("Invalid exit code: %d", state.ExitCode))
			}
			return
		}

		select {
		case <-timeout:
			j.setError(fmt.Errorf("Container reached timeout: %s", jobTimeout))
			if err := j.provider.ContainerStop(ctx, id); err != nil {
				log.WithError(err).Error("Failed stopping container")
			}
			return
		case <-time.After(time.Second * 5):
		}
	}
}

const logFilePattern = "/tmp/logs-%s"

func (j *Job) processLogs(ctx context.Context, id string) {
	log := j.log.WithField("container_id", id)
	log.Debug("Starting processing logs...")

	f, err := os.Create(fmt.Sprintf(logFilePattern, j.id))
	if err != nil {
		log.WithError(err).Error("Unable to open tempfile for log")
	}
	defer f.Close()

	r := j.provider.ContainerLogs(ctx, id)
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		line := scanner.Text()
		prog, ok := parseProgressLine(line)
		if ok {
			j.progress <- prog
		} else if f != nil {
			if _, err := f.WriteString(line + "\n"); err != nil {
				log.WithError(err).Error("Failed writing to logfile")
			}
		}
	}
	close(j.progress)
	if err := scanner.Err(); err != nil {
		log.WithError(err).Error("Failed processing logs")
	}
}

func parseProgressLine(line string) (interop.ProgressEntry, bool) {
	pr := interop.ProgressEntry{}
	parts_global := strings.Split(line, "\t")
	if len(parts_global) < 2 {
		return pr, false
	}
	parts_step := strings.Split(parts_global[0], ":")
	if len(parts_step) < 4 || !strings.HasSuffix(parts_step[0], "Progress") {
		return pr, false
	}

	// trimming values
	for i, val := range parts_step {
		parts_step[i] = strings.Trim(val, " ")
	}

	name := parts_step[1]
	progress, err := strconv.ParseUint(parts_step[2], 10, 64)
	if err != nil {
		return pr, false
	}
	max, err := strconv.ParseUint(parts_step[3], 10, 64)
	if err != nil {
		return pr, false
	}
	global, err := strconv.ParseFloat(strings.Trim(parts_global[1], " %"), 64)
	if err != nil {
		return pr, false
	}

	pr.StepName = name
	pr.StepProgress = progress
	pr.StepMax = max
	pr.Global = global
	return pr, true
}

func (j *Job) Stop(ctx context.Context) error {
	if j.providerID == "" || j.isDone() {
		return nil
	}

	defer func() {
		j.setDone()
	}()

	j.log.Debug("Stopping job...")
	defer j.log.Debug("Job stopped")
	return j.provider.ContainerStop(ctx, j.providerID)
}

func (j *Job) Done() <-chan struct{} {
	return j.done
}

func (j *Job) Logs(ctx context.Context) io.ReadCloser {
	if j.providerID == "" {
		r, wc := io.Pipe()
		wc.CloseWithError(errors.New("Container not running"))
		return r
	}

	fileName := fmt.Sprintf(logFilePattern, j.id)
	r, wc := io.Pipe()

	if j.isDone() {
		f, err := os.Open(fileName)
		if err != nil {
			wc.CloseWithError(err)
			return r
		}
		return f
	}

	t, err := tail.TailFile(fileName, tail.Config{
		Follow: true,
		ReOpen: true,
		Poll:   true,
	})
	if err != nil {
		wc.CloseWithError(err)
		return r
	}

	go func() {
		defer t.Stop()
		for {
			select {
			case <-ctx.Done():
				wc.Close()
				return
			case <-j.done:
				wc.Close()
				return
			case line := <-t.Lines:
				if line == nil {
					continue
				}
				if err := line.Err; err != nil {
					wc.CloseWithError(err)
					return
				}
				wc.Write([]byte(line.Text + "\n"))
			}
		}
	}()

	return r
}

func (j *Job) Progress() <-chan interop.ProgressEntry {
	return j.progress
}

type jsonPayload struct {
	ID    string `json:"id"`
	Done  bool   `json:"done"`
	Error string `json:"error,omitempty"`
}

func (j *Job) MarshalJSON() ([]byte, error) {
	pl := &jsonPayload{
		ID:   j.id,
		Done: j.isDone(),
	}

	j.errMutex.Lock()
	err := j.lastErr
	j.errMutex.Unlock()
	if err != nil {
		pl.Error = err.Error()
	}

	return json.Marshal(pl)
}

func (j *Job) isDone() bool {
	select {
	case <-j.done:
		return true
	default:
		return false
	}
}

func (j *Job) setDone() {
	if j.isDone() {
		return
	}
	close(j.done)
}

func (j *Job) setError(err error) {
	j.log.WithError(err).Debug("Job error set")
	j.errMutex.Lock()
	j.lastErr = err
	j.errMutex.Unlock()
}

const containerImage = "registry.gitlab.com/mwse/cura-engine-docker"

func containerCommand(serverURL string) []string {
	cmds := []string{
		"echo Collecting resources...",
		fmt.Sprintf("curl -s -f -o config.def.json %s/config", serverURL),
		fmt.Sprintf("curl -s -f -o model.stl %s/model", serverURL),
		"echo Starting Cura Engine for slicing...",
		"/opt/CuraEngine/CuraEngine slice -p -j config.def.json -o slice.gcode -l model.stl",
		"echo Slicing using Cura Engine finished.",
		"echo Uploading result...",
		fmt.Sprintf("curl -s --upload-file slice.gcode %s/result", serverURL),
	}
	cmd := strings.Join(cmds, " && ")
	cmd += " || echo Failed with exit code $?"
	return []string{
		"/bin/sh",
		"-c",
		cmd,
	}
}
