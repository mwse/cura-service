package engine

import (
	"bytes"
	"io"
	"net/http"
	"strings"
	"time"
)

func (j *Job) serveFile(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.NotFound(w, r)
		return
	}

	file := strings.Trim(r.URL.Path, "/")
	content, ok := j.files[file]
	if !ok {
		http.NotFound(w, r)
		return
	}

	rs := bytes.NewReader(content)
	http.ServeContent(w, r, file, time.Now(), rs)
}

func (j *Job) handleUploadResult(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPut {
		http.NotFound(w, r)
		return
	}

	buf := &bytes.Buffer{}
	if _, err := io.Copy(buf, r.Body); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	j.result = buf.Bytes()
	w.WriteHeader(http.StatusCreated)
}
