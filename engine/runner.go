package engine

import (
	"context"
	"fmt"
	"net/http"
	"strings"
	"sync"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/mwse/cura-service/engine/docker"
	"gitlab.com/mwse/cura-service/interop"
)

var _ interop.Engine = (*Runner)(nil)
var _ interop.ContainerRunner = (*Runner)(nil)

type Runner struct {
	closed  chan struct{}
	jobs    map[string]*Job
	urlBase string
	log     logrus.FieldLogger

	*docker.ContainerManager
}

func NewRunner(ctx context.Context, log logrus.FieldLogger, urlBase string) (*Runner, error) {
	ctx, cancel := context.WithCancel(ctx)
	mgmt, err := docker.NewContainerManager(ctx, log.WithField("provider", "docker"), containerImage)
	if err != nil {
		cancel()
		return nil, err
	}

	r := &Runner{
		closed:  make(chan struct{}),
		jobs:    make(map[string]*Job),
		urlBase: urlBase,
		log:     log,

		ContainerManager: mgmt,
	}

	go func() {
		<-r.closed
		cancel()
	}()

	return r, nil
}

func (r *Runner) RemoteURL() string {
	return r.urlBase
}

func (r *Runner) CreateJob() (interop.Job, error) {
	j, err := NewJob(r.log.WithField("component", "job"), r)
	if err != nil {
		return nil, errors.Wrap(err, "Failed creating job")
	}

	r.jobs[j.id] = j
	return j, nil
}

func (r *Runner) GetJob(id string) (interop.Job, error) {
	job, ok := r.jobs[id]
	if !ok {
		return nil, errors.New("Job not found")
	}
	return job, nil
}

func (r *Runner) RemoveJob(id string) error {
	job, ok := r.jobs[id]
	if !ok {
		return nil
	}
	select {
	case <-job.Done():
		delete(r.jobs, id)
	default:
		return errors.New("Cannot remove job which is not done")
	}
	return nil
}

func (r *Runner) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	segments := strings.Split(strings.Trim(req.URL.Path, "/"), "/")
	if len(segments) < 1 {
		http.NotFound(w, req)
		return
	}

	path := segments[0]
	job, ok := r.jobs[path]
	if !ok {
		http.NotFound(w, req)
		return
	}

	http.StripPrefix("/"+path, job).ServeHTTP(w, req)
}

func (r *Runner) Close(ctx context.Context) error {
	select {
	case <-r.closed:
		return nil
	default:
	}

	r.log.Info("Closing runner...")
	close(r.closed)

	errorChan := make(chan error, len(r.jobs))
	var wg sync.WaitGroup
	for _, job := range r.jobs {
		wg.Add(1)
		go func(job *Job) {
			if err := job.Stop(ctx); err != nil {
				errorChan <- err
			}
			wg.Done()
		}(job)
	}

	wg.Wait()
	if len(errorChan) > 0 {
		errors := make([]error, 0, len(errorChan))
		for err := range errorChan {
			errors = append(errors, err)
		}
		return fmt.Errorf("Got errors from cleanup routines: %v", errors)
	}
	r.log.Info("Runner shut down.")
	return nil
}
