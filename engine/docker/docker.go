package docker

import (
	"context"
	"io"
	"io/ioutil"
	"os"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/network"
	docker "github.com/docker/docker/client"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/mwse/cura-service/interop"
)

var _ interop.ContainerProvider = (*ContainerManager)(nil)

type ContainerManager struct {
	Client      *docker.Client
	log         logrus.FieldLogger
	networkName string
}

const networkNameEnv = "CURA_DOCKER_NETWORK"

func NewContainerManager(ctx context.Context, log logrus.FieldLogger, imageRef string) (*ContainerManager, error) {
	client, err := docker.NewEnvClient()
	if err != nil {
		return nil, errors.Wrap(err, "Unable to create container client")
	}
	log.WithField("docker_host", client.DaemonHost()).Info("Docker client created")

	d := &ContainerManager{
		Client:      client,
		log:         log,
		networkName: os.Getenv(networkNameEnv),
	}

	go func() {
		// Pull loop
		for {
			log.WithField("image_ref", imageRef).Debug("Pulling image...")
			if err := d.pullImage(ctx, imageRef); err != nil {
				log.WithError(err).Error("Image pull failed")
			} else {
				log.Debug("Image pull successful")
			}

			select {
			case <-ctx.Done():
				return
			case <-time.After(time.Minute):
			}
		}

	}()

	return d, nil
}

func (d *ContainerManager) ContainerStart(ctx context.Context, image string, cmd []string) (string, <-chan error) {
	errChan := make(chan error, 10)

	config := &container.Config{
		Image: image,
		Cmd:   cmd,
	}
	c, err := d.Client.ContainerCreate(ctx, config, &container.HostConfig{}, &network.NetworkingConfig{}, "")
	if err != nil {
		errChan <- err
		return "", errChan
	}
	log := d.log.WithField("container_id", c.ID)
	log.Debug("Container created")

	go func() {
		if d.networkName != "" {
			err := d.Client.NetworkConnect(ctx, d.networkName, c.ID, &network.EndpointSettings{})
			if err != nil {
				log.WithError(err).Error("Failed connecting network")
				errChan <- errors.Wrap(err, "Failed connecting network")
				return
			}
			log.WithField("network_name", d.networkName).Debug("Network connected")
		}

		if err := d.Client.ContainerStart(ctx, c.ID, types.ContainerStartOptions{}); err != nil {
			log.WithError(err).Error("Failed starting container")
			errChan <- errors.Wrap(err, "Failed connecting network")
			return
		}
		log.Debug("Container started")
		close(errChan)
	}()

	return c.ID, errChan
}

var stopTimeout = time.Second * 2

func (d *ContainerManager) ContainerStop(ctx context.Context, id string) error {
	if err := d.Client.ContainerStop(ctx, id, &stopTimeout); err != nil {
		return err
	}
	d.log.WithField("container_id", id).Debug("Container stopped")
	return nil
}

func (d *ContainerManager) ContainerRemove(ctx context.Context, id string) error {
	if err := d.Client.ContainerRemove(ctx, id, types.ContainerRemoveOptions{Force: true}); err != nil {
		return err
	}
	d.log.WithField("container_id", id).Debug("Container removed")
	return nil
}

func (d *ContainerManager) ContainerState(ctx context.Context, id string) (interop.ContainerState, error) {
	c, err := d.Client.ContainerInspect(ctx, id)
	if err != nil {
		return interop.ContainerState{}, err
	}

	return interop.ContainerState{
		Running:  c.State.Running,
		ExitCode: c.State.ExitCode,
	}, nil
}

func (d *ContainerManager) pullImage(ctx context.Context, image string) error {
	r, err := d.Client.ImagePull(ctx, image, types.ImagePullOptions{})
	if err != nil {
		return err
	}

	if _, err := ioutil.ReadAll(r); err != nil {
		return err
	}

	return nil
}

func (d *ContainerManager) ContainerLogs(ctx context.Context, id string) io.ReadCloser {
	r, err := d.Client.ContainerLogs(ctx, id, types.ContainerLogsOptions{
		Follow:     true,
		ShowStdout: true,
		ShowStderr: true,
	})
	if err != nil {
		r, w := io.Pipe()
		w.CloseWithError(err)
		return r
	}
	d.log.WithField("container_id", id).Debug("Streaming container logs...")
	return r
}
