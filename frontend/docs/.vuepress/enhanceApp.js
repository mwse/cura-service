import Vuex from 'vuex'
import store from './store/index'
import VueRouter from "vue-router"

export default ({
    Vue,
    options,
    router,
    siteData
}) => {
    Vue.use(Vuex),
    Vue.use(VueRouter)
    Vue.mixin({store: store})
}