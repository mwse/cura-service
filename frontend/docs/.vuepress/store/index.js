import Vue from 'vue'
import Vuex from 'vuex'
import { previewModel, sliceModel, subscribeLogs } from '../api.js'

Vue.use(Vuex)

const defs = {
  printers: {
    "anycubic": "Anycubic"
  },
  filaments: {
    "filament": "Orange"
  },
  bedadhesions: {
    "none": "None",
    "skirt": "Skirt",
    "brim": "Brim",
    "raft": "Raft"
  },
  supports: {
    "none": "None",
    "bedonly": "Bed only",
    "everywhere": "Everywhere",
    "tree": "Tree"
  },
}

export default new Vuex.Store({
	state: {
		isRunning: false,
		isDone: false,
		isUploadingPreview: false,
		currentJob: {
			id: null,
			preview: "",
			log: "",
		},
		settings: {
			printer: Object.keys(defs.printers)[0],
			filament: Object.keys(defs.filaments)[0],
			model: null,
			infill: 20,
			bedAdhesion: Object.keys(defs.bedadhesions)[0],
			support: Object.keys(defs.supports)[0],
		},
		defs
	},
	mutations: {
		// Running
		startSlice (state) {
			state.isRunning = true
			state.isDone = false
			state.currentJob.log = "upload model...\n\n"
		},
		finishSlice(state) {
			state.isRunning = false
			state.isDone = true
		},
		// Job
		setJob(state, job) {
			state.currentJob.id = job.id
			state.currentJob.log += "start slicer...\n\n"
		},
		addLog(state, chunk) {
			state.currentJob.log += chunk
		},
		// Preview
		startRenderPreview (state) {
			state.isUploadingPreview = true
		},
		endRenderPreview (state) {
			state.isUploadingPreview = false
		},
		setPreview(state, preview) {
			state.currentJob.preview = preview
		},
		// Settings
		setPrinter(state, printer) {
			state.settings.printer = printer
		},
		setFilament(state, filament) {
			state.settings.filament = filament
		},
		setModel(state, model) {
			state.settings.model = model
		},
		setInfill(state, infill) {
			state.settings.infill = infill
		},
		setBedAdhesion(state, bedAdhesion) {
			state.settings.bedAdhesion = bedAdhesion
		},
		setSupport(state, support) {
			state.settings.support = support
		}
	},
	actions: {
		async uploadPreview({ commit, state }) {
			if (state.settings.model == null) {
				return
			}

			commit('startRenderPreview')

			commit('setPreview', await previewModel(state.settings.model))

			commit('endRenderPreview')
		},
		async startProcess({ commit, state, dispatch }) {
			if (this.isRunning) {
				return;
			}

			commit('startSlice')

			console.log("Start slicer...")
			const job = await sliceModel(state.settings.printer, state.settings.filament, state.settings.model, state.settings.infill, state.settings.bedAdhesion, state.settings.support)

			console.log("Job ID", job.id)
			commit('setJob', job)

			dispatch('startOutput')
		},
	    async startOutput({ commit, state }) {
			console.log("Start subscribtion...")
			const subscribtion = await subscribeLogs(state.currentJob.id)

			if (subscribtion.error !== null) {
				commit('addLog', `\n\n${subscribtion.error}`)
				commit('finishSlice')
				return
			}

			const chunks = [];

			while (true) {
				const { done, value } = await subscribtion.reader.read();

				if (done) {
					commit('finishSlice')
					break;
				}

				const row = new TextDecoder("utf-8").decode(value);
				console.log(row)

				commit('addLog', row)
			}
		},
	}
})