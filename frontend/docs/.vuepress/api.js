export async function previewModel(model) {
	const ratio = window.devicePixelRatio || 1;
	const config = {
		width: 400 * ratio,
		height: 300 * ratio
	};

	const form = new FormData();
	form.set("model", model);
	form.set("config", JSON.stringify(config));

	const res = await fetch(`/api/render`, {
		method: "POST",
		body: form
	});
	if (!res.ok) {
		return "";
	}

	const blob = await res.blob();
	const urlCreator = window.URL || window.webkitURL;
	const imageUrl = urlCreator.createObjectURL(blob);

	return imageUrl
}

export async function subscribeLogs(jobID) {
	const res = await fetch(`/api/jobs/${jobID}/logs`);

	if (!res.ok) {
		const payload = await res.json();
		return {error: payload.error, reader: null};
	}

	const reader = res.body.getReader();
    
	return {error: null, reader: reader}
}

export async function sliceModel(printer, filament, model, infill, bedAdhesion, support) {
	const form = new FormData();
	form.set("model", model);
	form.set("config", JSON.stringify({
		version: 2,
		name: "Anycubic i3 Mega",
		inherits: "anycubic_i3_mega",
		metadata: {
			visible: true,
			author: "TheTobby",
			manufacturer: "Anycubic",
			file_formats: "text/x-gcode",
			platform: "anycubic_i3_mega_platform.stl",
			has_materials: true,
			has_machine_quality: true,
			preferred_quality_type: "normal",
			machine_extruder_trains: {
				"0": "anycubic_i3_mega_extruder_0"
			}
		},
		overrides: {
		}
	}));

	const res = await fetch("/api/slice", {
		method: "POST",
		body: form
	});

	const job = await res.json();

	return job
}