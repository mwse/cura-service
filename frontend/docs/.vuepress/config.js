module.exports = {
  postcss: {
    plugins: [require("tailwindcss"), require("autoprefixer")]
  },
  title: "Cura Cloud",
  description: "Just playing around",
  port: process.env.PORT || 8080,
};
