package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/mwse/cura-service/server"
)

const remoteHostEnv = "CURA_SERVICE_HOST"
const portEnv = "PORT"

func main() {
	port := os.Getenv(portEnv)
	if port == "" {
		port = "8080"
	}
	addr := ":" + port

	log := logrus.New()
	log.SetLevel(logrus.DebugLevel)

	remoteHost := "host.docker.internal:" + port
	if host, ok := os.LookupEnv(remoteHostEnv); ok {
		remoteHost = host
	}

	server, err := server.NewHTTPServer(log.WithField("component", "server"), addr, remoteHost)
	if err != nil {
		log.WithError(err).Error("Creating central server failed")
	}

	// shutdown logic
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)
	go func() {
		<-signals
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*2)
		defer cancel()
		log.Info("Shutting down server...")
		if err := server.Shutdown(ctx); err != nil {
			log.WithError(err).Error("Error on server shutdown")
			return
		}
		log.Info("Server shut down.")
	}()

	log.WithField("addr", addr).Info("Starting server...")
	if err := server.ListenAndServe(); err != http.ErrServerClosed {
		log.WithError(err).Error("Central server failed")
	}
}
