package api

import (
	"encoding/json"
	"io"
	"net/http"
	"strings"

	"gitlab.com/mwse/cura-service/interop"
)

func (a *API) jobHandler(w http.ResponseWriter, r *http.Request) {
	segments := strings.Split(strings.Trim(r.URL.Path, "/"), "/")
	if len(segments) < 1 {
		http.NotFound(w, r)
		return
	}

	jobID := segments[0]
	job, err := a.Runner.GetJob(jobID)
	if err != nil {
		http.NotFound(w, r)
		return
	}

	if len(segments) == 1 && r.Method == http.MethodGet {
		a.jobGet(w, r, job)
		return
	}

	if len(segments) == 2 && r.Method == http.MethodGet {
		switch segments[1] {
		case "result":
			a.jobResultGet(w, r, job)
			return
		case "wait":
			a.jobWait(w, r, job)
			return
		case "logs":
			a.jobLogsStream(w, r, job)
			return
		case "progress":
			a.jobProgressStream(w, r, job)
			return
		}
	}

	http.NotFound(w, r)
}

func (a *API) jobGet(w http.ResponseWriter, r *http.Request, job interop.Job) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Cache-Control", "no-cache")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(job)
}

func (a *API) jobResultGet(w http.ResponseWriter, r *http.Request, job interop.Job) {
	w.Header().Set("Content-Type", "text/x.gcode")
	w.Header().Set("Cache-Control", "no-cache")
	w.WriteHeader(http.StatusOK)
	io.Copy(w, job.Result())
}

func (a *API) jobWait(w http.ResponseWriter, r *http.Request, job interop.Job) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Cache-Control", "no-cache")
	w.Header().Set("Connection", "keep-alive")
	<-job.Done()
	json.NewEncoder(w).Encode(job)
}

func (a *API) jobLogsStream(w http.ResponseWriter, r *http.Request, job interop.Job) {
	w.Header().Set("Content-Type", "text/plain")
	w.Header().Set("Cache-Control", "no-cache")
	w.Header().Set("Connection", "keep-alive")
	reader := job.Logs(r.Context())
	if n, err := io.Copy(w, reader); err != nil {
		if n > 0 {
			a.log.WithError(err).Error("Failed streaming logs")
		} else {
			respondError(w, 500, err)
		}
	}
}

func (a *API) jobProgressStream(w http.ResponseWriter, r *http.Request, job interop.Job) {
	w.Header().Set("Content-Type", "application/x-ndjson")
	w.Header().Set("Cache-Control", "no-cache")
	w.Header().Set("Connection", "keep-alive")

	w.WriteHeader(http.StatusOK)
	enc := json.NewEncoder(w)
	for prog := range job.Progress() {
		enc.Encode(&prog)
	}
}
