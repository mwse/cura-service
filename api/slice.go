package api

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"net/http"

	"github.com/pkg/errors"
)

func (a *API) slice(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.NotFound(w, r)
		return
	}

	job, err := a.Runner.CreateJob()
	if err != nil {
		respondError(w, http.StatusInternalServerError, err)
		return
	}

	file, _, err := r.FormFile("model")
	if err != nil {
		respondError(w, http.StatusBadRequest, err)
		return
	}
	defer file.Close()

	buf := &bytes.Buffer{}
	if _, err := io.Copy(buf, file); err != nil {
		respondError(w, http.StatusInternalServerError, err)
		return
	}
	job.StoreFile("model", buf.Bytes())

	config := r.FormValue("config")
	if config == "" {
		respondError(w, http.StatusBadRequest, err)
		return
	}
	job.StoreFile("config", []byte(config))

	ctx := context.Background()
	if err := job.Start(ctx); err != nil {
		respondError(w, http.StatusInternalServerError, errors.Wrap(err, "Failed starting job"))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(job)
}
