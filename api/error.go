package api

import (
	"encoding/json"
	"net/http"
)

type errorResponse struct {
	ID    string `json:"id,omitempty"`
	Error string `json:"error"`
}

func respondError(w http.ResponseWriter, code int, err error) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(&errorResponse{Error: err.Error()})
}
