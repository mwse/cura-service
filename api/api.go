package api

import (
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/mwse/cura-service/interop"
)

type API struct {
	*http.ServeMux

	log    logrus.FieldLogger
	Runner interop.Engine
}

func NewAPI(log logrus.FieldLogger, runner interop.Engine) *API {
	mux := http.NewServeMux()

	a := &API{
		ServeMux: mux,
		Runner:   runner,
		log:      log,
	}

	mux.HandleFunc("/slice", a.slice)
	mux.Handle("/jobs/", http.StripPrefix("/jobs", http.HandlerFunc(a.jobHandler)))

	return a
}
